
var client;
$(function(){

  
  client = ZAFClient.init(function(){
    client.on('app.route.changed', onRouteChanged);
    client.trigger('clientReady');
  });

  function onRouteChanged(data) {
    search(client, data.appParams);
    updateInputs(data.appParams);
  }

  initializeFilters(client, function(client, args){ search(client, getQueryStringParams(args)) });
});

function showInfo(data) {
  var html = Handlebars.compile($('#custom-fields-template').html())(data);
  var $content = $('#content-table');
  $content.html(html);
  $('#results-table').DataTable({
    searching: false,
    paging: false,
    info: false,
    'order':[],
    'language': {
      "zeroRecords": "Nenhum resultado encontrado.",
      "paginate": {
        "first":      "Primeiro",
        "last":       "Último",
        "next":       ">",
        "previous":   "<"
    }
  },
   'drawCallback': function(){
      loading(false);
    }
  })
  .on('click', '.ticket-id-cell',function(){ 
    var ticketId = $(this).data('value') || -1;
    if (ticketId > 0) {
      client.invoke('routeTo', 'ticket', ticketId);
    }
   });

   $('.custom-pagination').find('.page-change').click(function(){
     var page = $(this).data('page');
     if (page && page > 0) {
        loading(true);
        $('body').scrollTop(0);
        loadByPage({ page: page });
     }
   });
}

function renderResponse(data, custom_tag, field_value) {
  var all_tickets = data.results || [];

  //format results
  var user_ids = [];
  var tickets = [];

  for (var i = 0; i < all_tickets.length; i++) {
    var ticket = all_tickets[i];
    var custom_values = getFieldsValues(ticket, custom_tag, field_value);
    var insert = custom_tag == 'email_cliente' || custom_tag == 'nome_cliente' 
    || (custom_tag == 'id_ticket' && ticket.id == field_value) || custom_values['_insert'] == true;

    if (insert) {
      
      if (user_ids.indexOf(ticket.requester_id) < 0) user_ids.push(ticket.requester_id);

      tickets.push({
        id: ticket.id,
        status: (ticket.status[0]).toUpperCase(),
        subject:ticket.subject,
        custom_values:custom_values,
        username: ticket.username,
        group_id:ticket.group_id,
        requester_id: ticket.requester_id
      });
    }
  }

  var info = { 
    page: currentParams.currentPage,  
    nextPage: currentParams.currentPage + 1 > currentParams.pages ? null : currentParams.currentPage + 1,
    prevPage: currentParams.currentPage == 1 ? null : currentParams.currentPage - 1,
    totalPages: currentParams.pages,
    tickets:tickets
  };

  //busca infos de usuarios
  client.request('/api/v2/users/show_many.json?ids=' + user_ids.join(',')).then(
    function(data){
      var users = data.users || [];
      for (var i = 0; i < users.length; i++) {
        for (var j = 0; j < tickets.length; j++) {
          if (tickets[j].requester_id == users[i].id) {
            tickets[j].username = users[i].name;
            tickets[j].email = users[i].email;
          }
        }
      }

      info.tickets = tickets;
      showInfo(info);
    },
    function(err){
      console.log('ERRO AO CARREGAR - users names');
      showInfo(info);
  });
}


function getFieldsValues(ticket, selected_field, field_value) {
  var custom_fields = ticket.custom_fields || [];
  selected_field = selected_field.split(' ');
  field_value = field_value.split(' ');
  var values = {};
  var insert = false;

  for (var i = 0; i < custom_fields.length; i++) {
    for (var j = 0; j < CUSTOM_FIELDS.length; j++) {
      if (custom_fields[i].id == CUSTOM_FIELDS[j].id) {
        values[CUSTOM_FIELDS[j].custom_tag] = getCustomFieldText(CUSTOM_FIELDS[j], custom_fields[i].value) || '-';
        
        //so insere se o valor buscado for o mesmo do custom field selecionado
        if (CUSTOM_FIELDS[j].custom_tag == selected_field[0])
          insert = custom_fields[i].value == field_value[0];
        else if (selected_field.length > 1 && CUSTOM_FIELDS[j].custom_tag == selected_field[1])
          insert = custom_fields[i].value == field_value[1];
        
        break;
      }
    }
  }
  values['_insert'] = insert;
  return values;
}



function updateInputs(params) 
{
  var opt_values = params.custom_field_select;
  var search_text = params.search_text;
  var sub_option_cd_val = params.sub_option_cd;
  var sub_option_transportadora_val = params.sub_option_transportadora;

  $('#custom-field-select').val(opt_values).trigger('change');
  
  var sub_option_cd = $('#sub-options-cd');
  var sub_option_transportadora = $('#sub-options-transportadora');
  var search_input = $('#search-text');

  sub_option_cd.val(sub_option_cd_val).trigger('change');
  sub_option_transportadora.val(sub_option_transportadora_val).trigger('change');
  search_input.val(search_text);

  sub_option_cd.next('.select2-container').hide();
  sub_option_transportadora.next('.select2-container').hide();
  search_input.hide();

  if (search_text && search_text != '') {
    search_input.show();
  }
  
  if (sub_option_cd_val){
    sub_option_cd.next('.select2-container').show();
  }

  if (sub_option_transportadora_val) {
    sub_option_transportadora.next('.select2-container').show();
  }
}


var currentParams = {};

function search (client, params, callback) {
  loading(true);
  
  var search_text = params.search_text;
  var opt_values = params.custom_field_select.split('|');
  var custom_field_id = opt_values && opt_values.length > 0 ? opt_values[0] : -1;
  var custom_tag = opt_values && opt_values.length > 1  ? opt_values[1] : '';
  var field_value = '';

  if (custom_tag == 'cd_entrega') {
    field_value = params.sub_option_cd;
  }
  else if (custom_tag == 'transportadora') {
    field_value = params.sub_option_transportadora;
  }
  else if (custom_tag == 'cd_entrega transportadora') {
    field_value = params.sub_option_cd + ' ' + params.sub_option_transportadora;
  }
  else
    field_value = search_text;

  var searchTerm = 'type:ticket fieldvalue:' + field_value;

  //se for id do ticket, buscar direto
  if (custom_tag == 'id_ticket') {
    searchTerm = field_value;
  }
  else if (custom_tag == 'nome_cliente' || custom_tag == 'email_cliente') {
    searchTerm = 'requester:' + field_value + ' type:ticket';
  }
  
  currentParams.query = searchTerm;
  currentParams.currentPage = 1;
  currentParams.nextPage = null;
  currentParams.prevPage = null;
  currentParams.pages = 0;
  currentParams.count = 0;
  currentParams.custom_tag = custom_tag;
  currentParams.field_value = field_value; 
 
  loadByPage({ page:1 });
}


function loadByPage(params) {

  loading(true);
  var query = currentParams.query;
  var custom_tag = currentParams.custom_tag;
  var field_value = currentParams.field_value;
  var page = params.page || 1;
  var per_page = 30;

  var queryString = 'query=' + query + '&per_page=' + per_page  +  '&page=' + page;
  load('/api/v2/search.json?' + queryString, function(result){
    
    currentParams.count = result.count;
    currentParams.pages = Math.ceil(result.count / per_page);
    currentParams.currentPage = page;
    result.pages = Math.ceil(result.count / per_page);

    renderResponse(result, custom_tag, field_value);

  });

  function load(url, callback) 
  {
    client.request(url).then(
      function(data){
        callback(data);
      },
      function(err){
        console.log('ERRO AO CARREGAR - ' + url);
        if (callback) callback({ count:0 });
      });
  }
}

function getCustomFieldText(field, ticketValue) {
  
  var valueText = '-';
  if (field.type == 'tagger') {
    for (var i = 0; i < field.options.length; i++) {
      if (field.options[i].value == ticketValue) {
        valueText = field.options[i].name;
        break;
      }
    }
  }
  else {
    valueText = ticketValue;
  }
  
  return valueText;
}

getQueryStringParams = query => {
  return query
      ? (/^[?#]/.test(query) ? query.slice(1) : query)
          .split('&')
          .reduce((params, param) => {
                  let [key, value] = param.split('=');
                  params[key] = value ? decodeURIComponent(value.replace(/\+/g, ' ')) : '';
                  return params;
              }, {}
          )
      : {}
};
