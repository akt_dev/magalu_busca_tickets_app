

$(function()
{
  var client = ZAFClient.init(function(){
    preloadNavbar(client)
  });
  
  client.invoke('resize', { width:'400px', height: '350px' });
  initializeFilters(client, newSearch);
});


function newSearch(client, args) {
  var navBarClientPromise = client.get('instances').then(function(instancesData) {
    var instances = instancesData.instances;
    for (var instanceGuid in instances) {
      if (instances[instanceGuid].location === 'nav_bar') {
        return client.instance(instanceGuid);
      }
    }
  });

  navBarClientPromise.then(function(navBarClient) {
    var params = 'search?' + args;
    client.invoke('routeTo', 'nav_bar', 'busca-de-tickets', params);
    client.invoke('popover', 'hide');
  });
}

function preloadNavbar(client) {
  var navBarClientPromise = client.get('instances').then(function(instancesData) {
    var instances = instancesData.instances;
    for (var instanceGuid in instances) {
      if (instances[instanceGuid].location === 'nav_bar') {
        return client.instance(instanceGuid);
      }
    }
  });

  navBarClientPromise.then(function(navBarClient) {
    navBarClient.invoke('preloadPane');
  });
}
