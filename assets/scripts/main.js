
function initializeFilters (client, onSearch, completeCallback) {
  loadCustomFields(client, function(data){
    render(data);
    
    if (completeCallback) completeCallback();
    
    $('form').submit(function(e){ 
      e.preventDefault(); 
      var formatted = $(this).serialize();
      if (onSearch) onSearch(client, formatted);
    });

    $('#custom-field-select').on('change', function(){
      var selectedTag = $(this).val().split('|')[1];
      renderSubSelect(selectedTag);
    })

  });
}


function render(customFields) {
  var templateHtml = $('#template-search-box').html();
  var template = Handlebars.compile(templateHtml);
  var context = {
    custom_fields: customFields
  };

  for (var i = 0; i < customFields.length; i++) {
    if (customFields[i].custom_tag == 'transportadora')
      context.transportadora_options = customFields[i].options;
    else if (customFields[i].custom_tag == 'cd_entrega')
      context.cd_options = customFields[i].options;
  }
  $('#content-search-box').html(template(context));
  $('#custom-field-select').select2({
    placeholder: "Filtro de Busca*",
    minimumResultsForSearch: -1,
  }); 
  
  $('#sub-options-cd').select2({
    language: {
      "noResults": function(){ return "Nenhum resultado encontrado."; }
    },
    placeholder: "Selecione um CD"
  }); 

  $('#sub-options-transportadora').select2({
    language: {
      "noResults": function(){ return "Nenhum resultado encontrado."; }
    },
    placeholder: "Selecione uma transportadora"
  }); 

  $('.sub-select-nav').next('.select2-container').hide();
}

function renderSubSelect(tag) {
  
  var input_text = $('#search-text');
  var cd_options = $('#sub-options-cd');
  var transportadora_options = $('#sub-options-transportadora');

  input_text.val('').hide();
  cd_options.next('.select2-container').hide();
  cd_options.val(null).trigger('change');
  transportadora_options.next('.select2-container').hide();
  transportadora_options.val(null).trigger('change');

  if (tag == 'cd_entrega') {
    cd_options.next('.select2-container').show();
  }
  else if (tag == 'transportadora') {
    transportadora_options.next('.select2-container').show();
  }
  else if (tag == 'cd_entrega transportadora') {
    cd_options.next('.select2-container').show();
    transportadora_options.next('.select2-container').show();
  }
  else {
    if (tag == 'email_cliente')
      input_text.attr('type', 'email');
    else
      input_text.attr('type', 'text');
    input_text.show();
  }
}

const FILTER_CUSTOM_FIELDS = [ 
  { id:360007697352, tag:'id_pedido' }, 
  { id:360011011172, tag:'cpf_cnpj' }, 
  { id:360011074672, tag:'cd_entrega' }, 
  { id:360011943551, tag:'transportadora' }
];

var CUSTOM_FIELDS = [];

//services
function loadCustomFields(client, callback) {
  client.request('/api/v2/ticket_fields.json').then(
    function(data) {
      var all_fields = data.ticket_fields || [];
      var fields_ids = FILTER_CUSTOM_FIELDS.map(fi => { return fi.id });
      select_fields = all_fields.filter(f => { return f.id == 0 || (fields_ids.indexOf(f.id) > -1 && f.active); });
      select_fields = select_fields.map(s => { return { id: s.id, title: s.title_in_portal, type:s.type, options: s.custom_field_options, custom_tag: getCustomTag(s.id) } });
      CUSTOM_FIELDS = select_fields;
      if (callback) callback(select_fields);
    },
    function(response) {
      if (callback) callback([]);
      console.log(response);
    }
  );

  function getCustomTag(id) {
    for (var i = 0; i < FILTER_CUSTOM_FIELDS.length; i++) {
      if (FILTER_CUSTOM_FIELDS[i]['id'] == id) 
        return FILTER_CUSTOM_FIELDS[i]['tag'];
    }

    return null;
  }
}

function loading(value) {
  var panel = $('#loading-panel');
  if (value) {
    panel.fadeIn(200, function(){ $('#content-table').hide(); });
  }
  else {
    $('#content-table').show(); 
    panel.fadeOut(200);
  }
}
